<?php
/**
 * @package Spoilerific
 * @version 0.1
 */
/*
Plugin Name: Spoilerific
Plugin URI: http://awswan.info/wp-plugins/spoilerific/
Description: Display spoiler warnings on entire posts/pages, or on specific post/page content.
Author: Dan Hunsaker
Version: 0.1
License: GPL2
*/

class Spoilerific
{
	protected static $screens = array( 'post', 'page' );

	public static function install()
	{
		add_option('spoilerific_warning_text', 'This content contains spoilers.  Proceed with caution.' );
	}

	public static function settings_init()
	{
		add_settings_field(
			'spoilerific_warning_text',
			'Spoiler Warning Text',
			array('Spoilerific', 'warning_text_callback'),
			'reading',
			'default'
		);

		register_setting( 'reading', 'spoilerific_warning_text' );
	}

	public static function warning_text_callback()
	{
		?>
		The message to display at the top of posts with spoilery content.<br />
		<textarea name="spoilerific_warning_text" id="spoilerific_warning_text" cols="50" rows="5" class="large-text"><?php echo get_option( 'spoilerific_warning_text', 'This content contains spoilers.  Proceed with caution.' ); ?></textarea>
		<?php
	}

	public static function add_meta_box()
	{
		foreach ( self::$screens as $screen )
		{
			add_meta_box(
				'spoilerific_is_spoilery',
				'Spoilerific',
				array( 'Spoilerific', 'meta_box_callback'),
				$screen,
				'side'
			);
		}
	}

	public static function meta_box_callback( $post )
	{
		wp_nonce_field( 'spoilerific_nonce_field', 'spoilerific_nonce' );

		$value = get_post_meta( $post->ID, '_spoilerific_is_spoilery', true );

		echo '<label>';
		echo '<input type="checkbox" id="spoilerific_is_spoilery" name="spoilerific_is_spoilery" value="1" ' . checked( $value, 1, false ) . ' />';
		echo " Display spoiler warning on this {$post->post_type}";
		echo '</label> ';
	}

	public static function hide_is_spoilery( $column, $post_id )
	{
	    if ( $column != 'likes' )
	    	return;

	    $value = get_post_meta( $post_id , '_spoilerific_is_spoilery' , true );

	    echo '<input type="hidden" id="spoilerific_is_spoilery" name="spoilerific_is_spoilery" value="' . $value . '" />';
	}

	public static function quick_edit( $column_name, $post_type )
	{
		if ( ! in_array( $post_type, self::$screens ) )
			return;

		if ( $column_name != 'likes' )
			return;

		wp_nonce_field( 'spoilerific_nonce_field', 'spoilerific_nonce' );

		?>
		<label class="inline-edit-group">
			<input type="checkbox" id="spoilerific_is_spoilery" name="spoilerific_is_spoilery" value="1" />
			<span class="title">Is Spoilery</span>
		</label>
		<?php
	}

	public static function save_is_spoilery( $post_id )
	{
		if ( ! isset( $_POST['spoilerific_nonce'] ) )
			return;
		if ( ! wp_verify_nonce( $_POST['spoilerific_nonce'], 'spoilerific_nonce_field' ) )
			return;

		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
			return;

		if ( ! ( isset( $_POST['post_type'] ) && current_user_can( "edit_{$_POST['post_type']}", $post_id ) ) )
			return;

		if ( ! isset( $_POST['spoilerific_is_spoilery'] ) )
			$_POST['spoilerific_is_spoilery'] = '0';

		update_post_meta( $post_id, '_spoilerific_is_spoilery', $_POST['spoilerific_is_spoilery'] );
	}

	public static function bulk_save_is_spoilery()
	{
		$post_ids = ( ! empty( $_POST[ 'post_ids' ] ) ) ? $_POST[ 'post_ids' ] : array();
		$spoilerific_is_spoilery = ( ! empty( $_POST[ 'spoilerific_is_spoilery' ] ) ) ? wp_kses_post( $_POST[ 'spoilerific_is_spoilery' ] ) : null;

		if ( ! empty( $post_ids ) && is_array( $post_ids ) )
		{
			foreach ( $post_ids as $post_id )
			{
				update_post_meta( $post_id, '_spoilerific_is_spoilery', $spoilerific_is_spoilery );
			}
		}

		exit();
	}

	public static function show_formats_dropdown( $buttons )
	{
		array_unshift( $buttons, 'styleselect' );
		return $buttons;
	}

	public static function add_new_formats( $settings )
	{
		// Create array of new styles
		$new_styles = array(
			array(
				'title'	=> 'Spoilerific',
				'items'	=> array(
					array(
						'title'		=> 'Spoiler Warning',
						'inline'	=> 'span',
						'classes'	=> 'spoilerific-warning'
					),
				),
			),
		);

		// Merge old & new styles
		$settings['style_formats_merge'] = true;

		// Add new styles
		$settings['style_formats'] = json_encode( $new_styles );

		// Return New Settings
		return $settings;
	}

	public static function show_warning( $content )
	{
		global $wp_current_filter;
		if ( !in_array( 'get_the_excerpt', (array) $wp_current_filter ) )
		{
			if ( !is_feed() && !is_attachment() )
			{
				if ( is_single() || is_page() || is_home() || is_archive() || is_search() || is_category() )
				{
					$html = '<p class="spoilerific-warning">' . get_option( 'spoilerific_warning_text', 'This content contains spoilers.  Proceed with caution.' ) . '</p>';
					$is_spoilery = (int) get_post_meta( get_the_ID(), '_spoilerific_is_spoilery', true );

					if ( $is_spoilery > 0 )
					{
						$content = $html . "\n" . $content;
					}
				}
			}
		}
		return $content;
	}

	public static function display_style()
	{
	    // Frontend - Register our stylesheet.
	    wp_register_style( 'spoilerificStylesheet', plugins_url('stylesheet.css', __FILE__) );
	    wp_enqueue_style( 'spoilerificStylesheet' );
	}

	public static function admin_scripts()
	{
	    // Admin - Register our script.
		wp_register_script( 'spoilerificScript', plugins_url('spoilerific.js', __FILE__), array( 'jquery', 'inline-edit-post' ), false, true );
		wp_enqueue_script( 'spoilerificScript' );
	}

	public static function editor_style( $mce_css )
	{
		if ( ! empty( $mce_css ) )
			$mce_css .= ',';

		$mce_css .= plugins_url( 'stylesheet.css', __FILE__ );

		return $mce_css;
	}
}

add_action( 'admin_init', array('Spoilerific', 'settings_init') );
add_action( 'manage_posts_custom_column' , array( 'Spoilerific', 'hide_is_spoilery'), 10, 2 );
add_action( 'bulk_edit_custom_box', array('Spoilerific', 'quick_edit'), 10, 2 );
add_action( 'quick_edit_custom_box', array('Spoilerific', 'quick_edit'), 10, 2 );
add_action( 'add_meta_boxes', array('Spoilerific', 'add_meta_box') );
add_action( 'save_post', array('Spoilerific', 'save_is_spoilery') );
add_action( 'wp_ajax_save_spoilerific_is_spoilery', array('Spoilerific', 'bulk_save_is_spoilery') );

add_action( 'wp_enqueue_scripts', array('Spoilerific', 'display_style') );
add_action( 'admin_enqueue_scripts', array('Spoilerific', 'admin_scripts') );
add_filter( 'mce_css', array('Spoilerific', 'editor_style') );

add_filter( 'mce_buttons_2', array('Spoilerific', 'show_formats_dropdown') );
add_filter( 'tiny_mce_before_init', array('Spoilerific', 'add_new_formats') );

add_filter( 'the_content', array('Spoilerific', 'show_warning') );

register_activation_hook( __FILE__, array( 'Spoilerific', 'install' ) );
