=== Spoilerific! ===
Contributors: danhunsaker
Tags: spoiler, spoilers, spoiler warning, spoilery, spoilery post
Requires at least: 3.0.1
Tested up to: 3.9.2
Stable tag: 0.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html



== Description ==



== Installation ==

Installation is simple:

1. Upload the `spoilerific/` directory to `/wp-content/plugins/`
1. Activate the plugin through the 'Plugins' menu in WordPress

And that's it!  Just deactivate the plugin to disable, and delete `spoilerific/` to uninstall. It is always a good idea to
disable plugins *before* uninstalling them.

== Frequently Asked Questions ==

None yet.

== Changelog ==

= 0.1 =
* Initial Version

== Upgrade Notice ==

= 0.1 =
This version is the original.
